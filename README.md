# Medición de oxígeno #

### ¿Qué hace el módulo de medición de oxígeno? ###

* Medir el oxígeno disuelto del agua
* Mostrar la medida en la pantalla de Autoquarium
* Activar o desactivar la bomba de oxígeno para que el acuario se mantenga en los niveles adecuados para los peces.